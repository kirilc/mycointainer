## nuls-io/nuls-v2

```bash
git clone https://github.com/nuls-io/nuls-v2.git
cd nuls-v2
```
## Build Docker image 
```bash
docker build -t nuls:v2.9.2 .
```

## Run docker container
```bash
docker run --name nuls-v-292 -d \
  -p 18001:18001 \
  -p 18002:18002 \
  -p 18004:18004 \
  -v `pwd`/data:/nuls/data \
  -v `pwd`/logs:/nuls/Logs \
  nuls:v2.9.2

```
## Or you can use docker-compose to build a nuls app.
```bash
wget https://gitlab.com/kirilc/nuls-v2-docker/-/blob/main/docker-compose.yaml
```
