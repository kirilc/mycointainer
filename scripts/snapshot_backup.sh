#!/bin/bash
  
time=`date +%Y-%m-%d_%H-%M-%S`

# Modify the following variables according to your installation
#########################################

day="`date +%d`";
ex="`expr $day % 2`";
if [ $ex == "0" ]
then
	backup_dir=/tmp/polkadot-backup/1
else
	backup_dir=/tmp/polkadot-backup/2
fi

# backup_dir - directory to backup to
#backup_dir=/tmp/polkadot-backup/$time

# vol_group - the Volume Group that contains $polkadot_vol
vol_group=mynew_vg

# polkadot_vol - the Logical Volume that contains polkadot data
polkadot_vol=vol01

# polkadot_vol_fs - the file system type (ext3, xfs, ...) in lvm 
polkadot_vol_fs=ext4

# lvcreate and lvremove commands path - 
#lvcreate_cmd=`/usr/bin/which lvcreate`
#lvremove_cmd=`/usr/bin/which lvremove`
lvcreate_cmd=/usr/sbin/lvcreate
lvremove_cmd=/usr/sbin/lvremove
# Do not change anything beyond this point
#########################################
# Test for an interactive shell
if [[ $- != *i* ]]
   then say() { echo -e $1; }
     # Colors, yo!
     GREEN="\e[1;32m"
     RED="\e[1;31m"
     CYAN="\e[1;36m"
     PURPLE="\e[1;35m"
   else say() { true; } # Do nothing
fi

say $lvcreate_cmd $lvremove_cmd "Test"
# Output date
say $GREEN"Backup started at "$RED"`date`"$GREEN"."


# Create a logical volume called PolkadotBackup
say $GREEN"Creating a LV called PolkadotBackup:"$PURPLE
$lvcreate_cmd -L4000M  -s -n PolkadotBackup /dev/$vol_group/$polkadot_vol
echo  "/dev/$vol_group/$polkadot_vol"

# Create a mountpoint to mount the logical volume to
say $GREEN"Creating a mountpoint for the LV..."
# WARNING: this is insecure!
mkdir -p /tmp/PolkadotBackup

# Mount the logical volume to the mountpoint
say $GREEN"Mounting the LV..."
# WARNING: remove nouuid option if the filesystem is not formatted as XFS !!!   
#mount -o ro
#echo dev/$vol_group/PolkadotBackup
/bin/mount  -o ro /dev/$vol_group/PolkadotBackup /tmp/PolkadotBackup/


# For testing only
#say $RED"Press Enter to continue...\e[0m"
#read input

# Create the current backup
say $GREEN"Creating the backup directory and backup..."
mkdir -p $backup_dir
tar -zcvf $backup_dir/polkadot.backup.tar.gz /tmp/PolkadotBackup/ 2&> /dev/null

# Unmount /tmp/PolkadotBackup and remove the logical volume
say $GREEN"Unmounting and removing the LV."$PURPLE
umount /tmp/PolkadotBackup/
$lvremove_cmd --force /dev/$vol_group/PolkadotBackup

# Done!
say $GREEN"Polkadot  backed up to "$CYAN$backup_dir$GREEN"!"
say $GREEN"Backup ended at "$RED"`date`"$GREEN".\e[0m"

