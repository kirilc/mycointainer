## Run polkadot in Docker image
```bash
mkdir -p  /my/local/folder/
chmod 777 -R  /my/local/folder/
docker run  -p 30333:30333  -p 9933:9933 -p 9944:9944 -v /my/local/folder/:/polkadot parity/polkadot --name "Alzheimer-lv"
```
## Run polkadot with docker composer
```bash
docker-compose up 
```
